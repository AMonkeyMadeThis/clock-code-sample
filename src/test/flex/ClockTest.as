package
{
	import org.hamcrest.assertThat;
	import org.hamcrest.object.equalTo;
	import org.hamcrest.object.isTrue;
	
	public class ClockTest
	{
		private var clock:IClock;
		
		[Before]
		public function before():void
		{
			clock = new Clock();
		}
		
		[After]
		public function after():void
		{
			clock = null;
		}
		
		[Test]
		public function prints_the_hour():void
		{
			assertThat(clock.at(8), equalTo('08:00'));
			assertThat(clock.at(9), equalTo('09:00'));
		}
		
		[Test]
		public function prints_past_the_hour():void
		{
			assertThat(clock.at(11, 9), equalTo('11:09'));
			assertThat(clock.at(11, 19), equalTo('11:19'));
		}
		
		[Test]
		public function can_add_minutes():void
		{
			assertThat(clock.at(10).plus(3), equalTo('10:03'));
		}
		
		[Test]
		public function can_add_over_an_hour():void
		{
			assertThat(clock.at(10).plus(61), equalTo('11:01'));
		}
		
		[Test]
		public function adds_past_midnight():void
		{
			assertThat(clock.at(23, 59).plus(2), equalTo('00:01'));
		}
		
		[Test]
		public function subtracts_minutes():void
		{
			assertThat(clock.at(10, 3).minus(3), equalTo('10:00'));
		}
		
		[Test]
		public function subtracts_hours():void
		{
			assertThat(clock.at(10, 3).minus(30), equalTo('09:33'));
			assertThat(clock.at(10, 3).minus(70), equalTo('08:53'));
		}
		
		[Test]
		public function knows_its_time_is_the_same_as_another_clock():void
		{
			var one:IClock = clock.at(10, 3);
			var two:IClock = clock.at(10, 3);
			
			assertThat(one.equals(two), isTrue());
		}
		
		[Test]
		public function knows_its_time_is_not_the_same_as_another_clock():void
		{
			var one:IClock = clock.at(10, 3);
			var two:IClock = clock.at(10, 4);
			
			assertThat(one.equals(two), isTrue());
		}
		
		[Test]
		public function rewinds_over_midnight():void
		{
			assertThat(clock.at(0, 3).minus(4), equalTo('23:59'));
		}
	}
}
