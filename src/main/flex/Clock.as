/**
 * Clock
 * 
 * @author Justin@AMonkeyMadeThis.co.uk
 * @created Jun 8, 2015
 */
package
{
	import mx.utils.StringUtil;
	
	import util.NumberUtils;
	import util.TimeUtils;

	/**
	 * Clock
	 */
	public class Clock implements IClock
	{
		// ---------------------------------------------------------------------
		//
		// Consts
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Pattern string used to format string representation of the clock
		 */
		public static const TIME_FORMAT:String = "{0}:{1}";
		
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// hours
		// ----------------------------------
		
		/**
		 * hours
		 */
		public function get hours():Number { return (TimeUtils.millisecondsToHours(time) + 24) % 24; }
		/**
		 * @private
		 */
		public function set hours(value:Number):void
		{
			if (hours != value)
			{
				at(value, minutes);
			}
		}
		
		// ----------------------------------
		// minutes
		// ----------------------------------
		
		/**
		 * minutes
		 */
		public function get minutes():Number { return TimeUtils.millisecondsToMinutes(time); }
		/**
		 * @private
		 */
		public function set minutes(value:Number):void
		{
			if (minutes != value)
			{
				at(hours, value);
			}
		}
		
		// ----------------------------------
		// time
		// ----------------------------------
		
		private var _time:Number = 0;
		
		/**
		 * time
		 */
		public function get time():Number { return _time; }
		/**
		 * @private
		 */
		public function set time(value:Number):void
		{
			if (_time != value)
			{
				// update value
				_time = value;
			}
		}
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// IClock
		// ----------------------------------
		
		/**
		 * Sets the clock to the given <code>hours</code> & <code>minutes</code>.
		 * 
		 * @param hours number of <code>hours</code> to initiate the clock with.
		 * @param minutes number of <code>minutes</code> to initiate the clock with (optional).
		 * @return the current instance of clock.
		 */
		public function at(hours:uint, minutes:uint=0):IClock
		{
			var _hoursInMilliseconds:Number = TimeUtils.hoursToMilliseconds(hours);
			var _minutesInMilliseconds:Number = TimeUtils.minutesToMilliseconds(minutes);
			
			time = _hoursInMilliseconds + _minutesInMilliseconds;
			
			return this;
		}
		
		/**
		 * Adds a number of <code>minutes</code> to the clock.
		 * 
		 * @param minutes the number of <code>minutes</code> to increment the clocks time.
		 * @return the current instance of clock.
		 */
		public function plus(minutes:uint):IClock
		{
			this.time += TimeUtils.minutesToMilliseconds(minutes);
			
			return this;
		}
		
		/**
		 * Subtracts a number of <code>minutes</code> from the clock.
		 * 
		 * @param minutes the number of <code>minutes</code> to decrement the clocks time.
		 * @return the current instance of clock.
		 */
		public function minus(minutes:uint):IClock
		{
			this.time -= TimeUtils.minutesToMilliseconds(minutes);
			
			return this;
		}
		
		/**
		 * Compares this clocks time against another.
		 * 
		 * @param target the other clock used for comparison.
		 * @return the result of the comparison.
		 */
		public function equals(target:IClock):Boolean
		{
			if (!target)
				return false;
			
			return this.time == target.time;
		}
		
		/**
		 * Outputs a text representation
		 * 
		 * @return a string representation of this clock.
		 */
		public function toString():String
		{
			return StringUtil.substitute(TIME_FORMAT, NumberUtils.padLeadingZeros(hours), NumberUtils.padLeadingZeros(minutes));
		}
	}
}