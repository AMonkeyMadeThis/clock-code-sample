/**
 * TimeUtils
 * 
 * @author Justin@AMonkeyMadeThis.co.uk
 * @created Jun 8, 2015
 */
package util
{
	/**
	 * TimeUtils
	 */
	public class TimeUtils
	{
		// ---------------------------------------------------------------------
		//
		// Consts
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Number of milliseconds in an hour
		 */
		public static const MILLISECONDS_IN_HOUR:Number = 1000 * 60 * 60;
		/**
		 * Number of milliseconds in a minute
		 */
		public static const MILLISECONDS_IN_MINUTE:Number = 1000 * 60;
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// Hours
		// ----------------------------------
		
		/**
		 * Convert a number of hours into milliseconds
		 */
		public static function hoursToMilliseconds(hours:Number):Number
		{
			return hours * MILLISECONDS_IN_HOUR;
		}
		
		/**
		 * Convert a number of milliseconds into hours 
		 */
		public static function millisecondsToHours(milliseconds:Number):Number
		{
			return Math.floor(milliseconds / MILLISECONDS_IN_HOUR);
		}
		
		// ----------------------------------
		// Minutes
		// ----------------------------------
		
		/**
		 * Convert a number of minutes into milliseconds
		 */
		public static function minutesToMilliseconds(minutes:Number):Number
		{
			return minutes * MILLISECONDS_IN_MINUTE;
		}
		
		/**
		 * Convert a number of milliseconds into minutes
		 */
		public static function millisecondsToMinutes(milliseconds:Number):Number
		{
			return milliseconds / MILLISECONDS_IN_MINUTE - (millisecondsToHours(milliseconds)*60);
		}
	}
}