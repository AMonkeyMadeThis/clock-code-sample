/**
 * NumberUtils
 * 
 * @author Justin@AMonkeyMadeThis.co.uk
 * @created Jun 8, 2015
 */
package util
{
	/**
	 * NumberUtils
	 */
	public class NumberUtils
	{
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Pads a number string to a target length by prepending leading zeros.
		 */
		public static function padLeadingZeros(input:Number, length:uint=2):String
		{
			var _result:String = input.toString();
			
			while(_result.length < length)
			{
				_result = "0" + _result;
			}
			
			return _result;
		}
	}
}