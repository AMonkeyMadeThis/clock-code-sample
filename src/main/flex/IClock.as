/**
 * IClock
 * 
 * @author Justin@AMonkeyMadeThis.co.uk
 * @since Jun 8, 2015
 */
package
{
	/**
	 * IClock
	 */
	public interface IClock
	{
		// ---------------------------------------------------------------------
		//
		// Vars
		//
		// ---------------------------------------------------------------------
		
		// ----------------------------------
		// hours
		// ----------------------------------
		
		/**
		 * hours
		 */
		function get hours():Number;
		/**
		 * @private
		 */
		function set hours(value:Number):void;
		
		// ----------------------------------
		// minutes
		// ----------------------------------
		
		/**
		 * minutes
		 */
		function get minutes():Number;
		/**
		 * @private
		 */
		function set minutes(value:Number):void;
		
		// ----------------------------------
		// time
		// ----------------------------------
		
		/**
		 * time
		 */
		function get time():Number;
		/**
		 * @private
		 */
		function set time(value:Number):void;
		
		// ---------------------------------------------------------------------
		//
		// Methods
		//
		// ---------------------------------------------------------------------
		
		/**
		 * Sets the clock to the given <code>hours</code> & <code>minutes</code>.
		 * 
		 * @param hours number of <code>hours</code> to initiate the clock with.
		 * @param minutes number of <code>minutes</code> to initiate the clock with (optional).
		 * @return the current instance of clock.
		 */
		function at(hours:uint, minutes:uint=0):IClock;
		/**
		 * Adds a number of <code>minutes</code> to the clock.
		 * 
		 * @param minutes the number of <code>minutes</code> to increment the clocks time.
		 * @return the current instance of clock.
		 */
		function plus(minutes:uint):IClock;
		/**
		 * Subtracts a number of <code>minutes</code> from the clock.
		 * 
		 * @param minutes the number of <code>minutes</code> to decrement the clocks time.
		 * @return the current instance of clock.
		 */
		function minus(minutes:uint):IClock;
		/**
		 * Compares this clocks time against another.
		 * 
		 * @param target the other clock used for comparison.
		 * @return the result of the comparison.
		 */
		function equals(target:IClock):Boolean;
		/**
		 * Outputs a text representation
		 * 
		 * @return a string representation of this clock.
		 */
		function toString():String;
	}
}