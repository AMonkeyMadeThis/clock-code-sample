ClockTest

A simple TDD code interview test, write a 'clock' class that allows the provided ClockTest suite class to run. The tests involve adding or subtracting to the clock's time and asserting that it is done correctly.

The Clock class I produced functions in the same way as the in-built Date class in that it maintains the 'time' rather than a number of hours, minutes and seconds although it does not use the Date class to do this.